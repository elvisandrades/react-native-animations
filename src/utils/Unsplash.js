import { createApi } from "unsplash-js";
import nodeFetch from 'node-fetch';
import { ACCESS_KEY }  from './constants';
// on your node server
const unsplash = createApi({
  accessKey: ACCESS_KEY,
  fetch: nodeFetch,
});

export default unsplash;