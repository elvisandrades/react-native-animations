import React, { useState, useEffect } from 'react';
import {
  Animated,
  Easing,
  FlatList,
  Image,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  Dimensions
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getRandomImages } from "../../redux/images/actions";
import { imgsReducer } from "../../redux/images/reducers";
import { LinearGradient } from "expo-linear-gradient";
import Header from "../../components/Header";
import { getPhotoTitle } from "../../utils/helperFunctions";
import { SCREEN_WIDTH, SCREEN_HEIGHT, NAVBAR_HEIGHT, STATUS_BAR_HEIGHT, HORIZONTAL_PADDING } from "../../utils/constants";
const { width, height } = Dimensions.get('screen');
const opacityMin = 0;
const translateYMin = 60;
const translateXMin = 90;
const scrollXMin = 0;

const ITEM_WIDTH = width * 0.76;
const ITEM_HEIGHT = width * 1.47;
 
const ImageDetail = props => {
  const { infoVisible, currentImage, goToProfile, index , translateYValue, movementType, translateXValue, scrollXValue} = props;
  const animatedStyle = {
    opacity: props.opacityValue,
    transform: [movementType === 'up' ? { translateY: translateYValue} : { translateX: translateXValue}]
  };
  const inputRange = [
    (index - 1) * width,
    (index - 0.5) * width,
    index * width,
    (index + 0.5) * width,
    (index + 1) * width
  ];
  
  const translateXScroll = scrollXValue.interpolate({
    inputRange,
    outputRange: [-width * .7,-width * .35, 0, width * .35,width * .7]
  });
  return (
    <View style={[styles.flx1, styles.centerContent,{ overflow: "hidden", borderRadius: 18, borderLeftWidth: 5,borderRightWidth: 5,
    borderColor: 'white', shadowColor: '#000', shadowOffset: { width: 0, height: 0}, shadowOpacity: 1 } ]}>
      <Animated.Image
        style={[styles.flx1, {width: width * 1.4, resizeMode: "cover", transform: [{ translateX: translateXScroll}]}]}
        source={{ uri: currentImage.urls.regular }}
      />

   <LinearGradient
          colors={["transparent", "rgba(0,0,0,0.9)"]}
          start={[0.5, 0.7]}
          style={[styles.absoluteTop, styles.fullFill]}
        />

 {infoVisible && <ImageInfo
            image={currentImage}
            animatedStyle={animatedStyle}
            goProfile={() => goToProfile(currentImage)}
          />}
        

        <TouchableOpacity
          style={styles.profileTouchable}
          activeOpacity={0.9}
          onPress={() => props.effectGoUp()}
        >
          <View style={styles.flx1} />
        </TouchableOpacity>
    </View>
  );
};

const ImageInfo = props => (
  <Animated.View style={[styles.detailInfo, props.animatedStyle]}>
    <Text style={styles.detailPhotoTitle}>{getPhotoTitle(props.image)}</Text>
    <Text style={styles.detailLikes}>{`${props.image.likes} likes`}</Text>
    <TouchableOpacity
      style={styles.profileContainer}
      onPress={() => props.goProfile()}
    >
      <Image
        style={styles.userImage}
        source={{
          uri: props.image.user.profile_image.medium
        }}
      />
      <View style={{ paddingLeft: 8, justifyContent: "center" }}>
        <Text style={styles.detailUserName}>{props.image.user.name}</Text>
        <Text style={styles.viewProfile}>View profile</Text>
      </View>
    </TouchableOpacity>
  </Animated.View>
);

class DetailScreen extends React.Component {
  
  state = {
    currentIndex: this.props.route.params.index,
    infoVisible: false,
    opacityValue: new Animated.Value(opacityMin),
    translateYValue: new Animated.Value(translateYMin),
    translateXValue: new Animated.Value(translateXMin),
    scrollXValue: new Animated.Value(scrollXMin),
    movementType: 'up'
  };

  effectGoUp = () => {
    this.setState({ infoVisible: true, movementType: 'up' });
    Animated.parallel([
      Animated.timing(this.state.opacityValue, {
        toValue: 1,
        duration: 1000,
        easing: Easing.in(),
        useNativeDriver: false
      }),
      Animated.timing(this.state.translateYValue, {
        toValue: 0,
        duration: 900,
        easing: Easing.in(),
        useNativeDriver: false
      })
    ]).start();
  };

  hide = func => {
    Animated.parallel([
      Animated.timing(this.state.opacityValue, { toValue: opacityMin, useNativeDriver: false }),
      Animated.timing(this.state.translateYValue, {
        toValue: translateYMin,
        duration: 0,
        useNativeDriver: false
      })
    ]).start(func);
    this.setState({ infoVisible: false });
  };

  resetEfects = () => {
    Animated.parallel([
      Animated.timing(this.state.opacityValue, {
        toValue: opacityMin,
        duration: 0,
        useNativeDriver: false
      }),
      Animated.timing(this.state.translateYValue, {
        toValue: translateYMin,
        duration: 0,
        useNativeDriver: false
      }),
      Animated.timing(this.state.translateXValue, {
        toValue: translateXMin,
        duration: 0,
        useNativeDriver: false
      })
    ]).start(({ finished }) => {
      Animated.parallel([
        Animated.timing(this.state.opacityValue, {
          toValue: 1,
          duration: 1000,
          easing: Easing.in(),
          useNativeDriver: false
        }),
        Animated.timing(this.state.translateYValue, {
          toValue: 0,
          duration: 900,
          easing: Easing.in(),
          useNativeDriver: false
        }),
        Animated.timing(this.state.translateXValue, {
          toValue: 0,
          duration: 900,
          easing: Easing.in(),
          useNativeDriver: false
        })
      ]).start();
    });
  };

  close() {
    this.hide(() => this.props.navigation.goBack());
  }

  goToProfile = item => {
    this.hide(() => this.props.navigation.navigate("Profile", { item }));
  };

  componentDidUpdate(prevProps) {
    if (this.props.route.params.index !== prevProps.route.params.index) {
      try {
        this.flatListRef.scrollToIndex({index: this.props.route.params.index});
      } catch (error) {
        console.log('error : ' +  error);
      }
    
     this.setState({
      currentIndex: this.props.route.params.index
     })
    }
  }

  render() {
    const { navigation, images, profileImages, route } = this.props;
    const {
      infoVisible,
      opacityValue,
      translateYValue,
      translateXValue, 
      scrollXValue
    } = this.state;
    const data =
    route.params.data === "random"
        ? images.images
        : profileImages.profileImages.results;
    return (
      <View style={styles.flx1}>
        <Animated.FlatList
          ref={ref => (this.flatListRef = ref)}
          data={data}
          pagingEnabled
          keyExtractor={(item, index) => item + index}
          renderItem={({ item, index }) => (
            <View style={styles.fullFill}>
              <ImageDetail
                index={index}
                currentImage={item}
                goToProfile={() => this.goToProfile(item)}
                effectGoUp={this.effectGoUp}
                infoVisible={infoVisible}
                opacityValue={opacityValue}
                translateYValue={translateYValue}
                translateXValue={translateXValue}
                scrollXValue={scrollXValue}
                movementType={this.state.movementType}
              />
            </View>
          )}
          horizontal
          showsHorizontalScrollIndicator={false}
          initialScrollIndex={this.state.currentIndex}
          getItemLayout={(data, index) => ({
            length: SCREEN_WIDTH,
            offset: SCREEN_WIDTH * index,
            index
          })}
          decelerationRate="fast"
          onScroll={
            Animated.event(
              [{ nativeEvent: { contentOffset: { x: scrollXValue } } }],
              { useNativeDriver: false }
            )
          }
          onTouchStart={()=> {
            Animated.timing(this.state.opacityValue, {
              toValue: opacityMin,
              duration: 0,
              useNativeDriver: false
            }).start();
            if(this.state.infoVisible) {
            this.setState({
              movementType: 'side'
            })
            }
          }
          }
          onMomentumScrollEnd={
            ()=> {
                this.resetEfects();
            }
          }
        />
        <Header
          icon="closeWhite"
          onPress={() => this.close()}
          extraStyles={styles.header}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { images, profileImages } = state;
  return { images, profileImages };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getImages: getRandomImages,
      imgsReducer
    },
    dispatch
  );

const Detail = connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailScreen);

export default Detail;

const styles = StyleSheet.create({
    flx1: { flex: 1},
    centerContent: { justifyContent: "center" },
  endContent: { justifyContent: "flex-end" },
  startContent: { justifyContent: "flex-start" },
  centerItems: { alignItems: "center" },
    fullFill: {
        height: height,
        width: SCREEN_WIDTH
      },
      header: {
        height: NAVBAR_HEIGHT,
        paddingTop: STATUS_BAR_HEIGHT
    },
      absoluteTop: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0
      },
      profileTouchable: {
        position: "absolute",
        top: 50,
        bottom: 90,
        left: 50,
        right: 50,
      },
      detailInfo: {
        padding: HORIZONTAL_PADDING,
        position: 'absolute',
        bottom: 10
      },
      detailPhotoTitle: {
        fontFamily: "MuseoMedium",
        fontSize: 42,
        lineHeight: 49,
        color: "#fff"
      },
      detailLikes: {
        fontFamily: "MuseoLight",
        fontSize: 14,
        lineHeight: 16,
        color: "#fff"
      },
      detailUserName: {
        fontFamily: "MuseoMedium",
        fontSize: 12,
        lineHeight: 14,
        paddingBottom: 8,
        color: "#fff"
      },
      viewProfile: {
        fontFamily: "MuseoRegular",
        fontSize: 10,
        lineHeight: 12,
        color: "#fff"
      },
      //-----** Profile **-----//
    profileContainer: {
    flexDirection: "row",
    paddingTop: 26,
    paddingLeft: HORIZONTAL_PADDING,
    marginRight: HORIZONTAL_PADDING,
    marginBottom: 30
  },
  userImage: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
});

