import React from "react";
import { Animated, Image, Text, TouchableOpacity, View, StyleSheet } from "react-native";
import Images from "../utils/Images";
import {SafeAreaView } from 'react-native-safe-area-context';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  HORIZONTAL_PADDING,
} from "../utils/constants";

export default class Header extends React.Component {
  render() {
    return (
      <Animated.View
        style={[
          styles.absoluteTop,
          styles.headerContainer,
          this.props.icon !== "closeWhite" && { backgroundColor: "#fff" },
          this.props.extraStyles
        ]}
      >
        <SafeAreaView>
          {this.props.icon == "menu" ? (
            <View style={styles.iconsContainer}>
              <TouchableOpacity onPress={() => this.props.onPress()}>
                <Animated.Image
                  style={this.props.opacity}
                  source={Images[this.props.icon]}
                />
              </TouchableOpacity>
              <Animated.Text style={[styles.pageTitle, this.props.opacity]}>
                Discover
              </Animated.Text>
              <View style={{ width: 25 }} />
            </View>
          ) : (
            <View style={styles.iconsContainer}>
              <TouchableOpacity onPress={() => this.props.onPress()}>
                <Image source={Images[this.props.icon]} />
              </TouchableOpacity>
            </View>
          )}
        </SafeAreaView>
      </Animated.View>
    );
  }
}
const styles = StyleSheet.create({
  absoluteTop: { 
    position: "absolute",
    top: 0,
    left: 0,
    right: 0
   },
   headerContainer: {
    justifyContent: "center",
    marginHorizontal: HORIZONTAL_PADDING,
    paddingTop: hp("4%")
  },
  iconsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    margin: 5
  },
  pageTitle: {
    fontFamily: "MuseoBlack",
    fontSize: 24,
    lineHeight: 28
  },
});
