import React from "react";
import { Animated, FlatList, View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getRandomImages } from "../../redux/images/actions";
import { imgsReducer } from "../../redux/images/actions";
import Header from "../../components/Header";
import { ImageListItem } from "../../components/ImageList";
import {
  NAVBAR_HEIGHT,
  SCREEN_HEIGHT,
  SCREEN_WIDTH,
  STATUS_BAR_HEIGHT
} from "../../utils/constants";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import Spinner from 'react-native-loading-spinner-overlay';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

class FeeScreen extends React.Component {
  scrollAnim = new Animated.Value(0);
  offsetAnim = new Animated.Value(0);
  state = {
    scrollAnim: this.scrollAnim,
    offsetAnim: this.offsetAnim,
    clampedScroll: Animated.diffClamp(
      Animated.add(
        this.scrollAnim.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1],
          extrapolateLeft: "clamp"
        }),
        this.offsetAnim
      ),
      0,
      NAVBAR_HEIGHT - STATUS_BAR_HEIGHT
    )
  };

  _clampedScrollValue = 0;
  _offsetValue = 0;
  _scrollValue = 0;

  componentDidMount() {
    this.props.getImages();

    this.state.scrollAnim.addListener(({ value }) => {
      const diff = value - this._scrollValue;
      this._scrollValue = value;
      this._clampedScrollValue = Math.min(
        Math.max(this._clampedScrollValue + diff, 0),
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT
      );
    });
    this.state.offsetAnim.addListener(({ value }) => {
      this._offsetValue = value;
    });
  }

  componentWillUnmount() {
    this.state.scrollAnim.removeAllListeners();
    this.state.offsetAnim.removeAllListeners();
  }

  _onScrollEndDrag = () => {
    this._scrollEndTimer = setTimeout(this._onMomentumScrollEnd, 250);
  };

  _onMomentumScrollBegin = () => {
    clearTimeout(this._scrollEndTimer);
  };

  _onMomentumScrollEnd = () => {
    const toValue =
      this._scrollValue > NAVBAR_HEIGHT &&
      this._clampedScrollValue > (NAVBAR_HEIGHT - STATUS_BAR_HEIGHT) / 2
        ? this._offsetValue + NAVBAR_HEIGHT
        : this._offsetValue - NAVBAR_HEIGHT;

    Animated.timing(this.state.offsetAnim, {
      toValue,
      duration: 350,
      useNativeDriver: false
    }).start();
  };

  render() {
    const { navigation } = this.props;
    const { clampedScroll } = this.state;

    const navbarTranslate = clampedScroll.interpolate({
      inputRange: [0, NAVBAR_HEIGHT - STATUS_BAR_HEIGHT],
      outputRange: [0, -(NAVBAR_HEIGHT - STATUS_BAR_HEIGHT)],
      extrapolate: "clamp"
    });
    const navbarOpacity = clampedScroll.interpolate({
      inputRange: [0, NAVBAR_HEIGHT - STATUS_BAR_HEIGHT],
      outputRange: [1, 0],
      extrapolate: "clamp"
    });
    return (
      <View style={[styles.flx1, styles.centerContent]}>
        <AnimatedFlatList
          numColumns={2}
          data={this.props.images.images}
          keyExtractor={(item, index) => item + index}
          renderItem={({ item, index }) => (
            <ImageListItem
              item={item}
              index={index}
              navigate={() =>
                navigation.navigate("Detail", { data: "random", index: index })
              }
            />
          )}
          columnWrapperStyle={styles.columnWrapperStyle}
          contentContainerStyle={{
            paddingTop: hp("15%"),
            marginHorizontal: 26
          }}
          scrollEventThrottle={1}
          onMomentumScrollBegin={this._onMomentumScrollBegin}
          onMomentumScrollEnd={this._onMomentumScrollEnd}
          onScrollEndDrag={this._onScrollEndDrag}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
            { useNativeDriver: false }
          )}
        />
 <Spinner
          visible={this.props.images.images.length === 0}
          color={"black"}
        />
<Header
          icon="menu"
          onPress={() => navigation.openDrawer()}
          extraStyles={[
            styles.header,
            { transform: [{ translateY: navbarTranslate }] }
          ]}
          opacity={{ opacity: navbarOpacity }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { images } = state;
  return { images };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getImages: getRandomImages,
      imgsReducer
    },
    dispatch
  );

const Fee = connect(
  mapStateToProps,
  mapDispatchToProps
)(FeeScreen);

export default Fee;

const styles = StyleSheet.create({
  flx1: { flex: 1, backgroundColor: 'white' },
  centerContent: { justifyContent: "center" },
  endContent: { justifyContent: "flex-end" },
  startContent: { justifyContent: "flex-start" },
  centerItems: { alignItems: "center" },
  fullFill: {
    height: SCREEN_HEIGHT,
    width: SCREEN_WIDTH
  },
  absoluteTop: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0
  },
  columnWrapperStyle: {
      flex: 1,
      justifyContent: "space-between"
  },
  header: {
      height: NAVBAR_HEIGHT,
      paddingTop: STATUS_BAR_HEIGHT
  },
  contentContainerStyle: {
      marginTop: hp("14%"),
      paddingTop: 10,
      marginHorizontal: 26
    },
    profileTouchable: {
      position: "absolute",
      top: 90,
      bottom: 90,
      left: 90,
      right: 90
    },
    userImage: {
      width: 40,
      height: 40,
      borderRadius: 20
    },
    spinnerTextStyle: {
      color: 'black'
    }
});