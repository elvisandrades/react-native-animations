import { StatusBar } from 'expo-status-bar';
import * as React from 'react';
import { useCallback, useEffect, useState } from 'react';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Entypo from '@expo/vector-icons/Entypo';
import * as SplashScreen from 'expo-splash-screen';
import * as Font from 'expo-font';
import "react-native-gesture-handler";
import 'react-native-url-polyfill/auto';
import AppContainer from "./src/navigation/Navigator";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from "react-redux";
import store from "./src/redux/images/store";

const Stack = createNativeStackNavigator();

export default function App() {
  const [appIsReady, setAppIsReady] = useState(false);

  useEffect(() => {
    async function prepare() {
      try {
        // Pre-load fonts, make any API calls you need to do here
        await Font.loadAsync(Entypo.font);
        await Font.loadAsync({
          MuseoLight: require("./src/assets/fonts/MuseoSans-100.otf"),
          MuseoRegular: require("./src/assets/fonts/MuseoSans-300.otf"),
          MuseoMedium: require("./src/assets/fonts/MuseoSans-500.otf"),
          MuseoBold: require("./src/assets/fonts/MuseoSans-700.otf"),
          MuseoBlack: require("./src/assets/fonts/MuseoSans-900.otf"),
          MuseoItalicLight: require("./src/assets/fonts/MuseoSans-100Italic.otf"),
          MuseoItalicRegular: require("./src/assets/fonts/MuseoSans-300Italic.otf"),
          MuseoItalicMedium: require("./src/assets/fonts/MuseoSans-500Italic.otf"),
          MuseoItalicBold: require("./src/assets/fonts/MuseoSans-700Italic.otf"),
          MuseoItalicBlack: require("./src/assets/fonts/MuseoSans-900Italic.otf")
        })
      } catch (e) {
        console.warn(e);
      } finally {
        // Tell the application to render
        setAppIsReady(true);
      }
    }

    prepare();
  }, []);

  const onLayoutRootView = useCallback(async () => {
    if (appIsReady) {
      await SplashScreen.hideAsync();
    }
  }, [appIsReady]);

  if (!appIsReady) {
    return null;
  }

  return ( <Provider store={store}>
    <SafeAreaProvider>
            <AppContainer />
            </SafeAreaProvider>
    </Provider>);
}