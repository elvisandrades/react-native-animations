import React, { useRef, useState } from "react";
import {
  StyleSheet,
  ImageBackground,
  Text,
  TouchableOpacity,
  Animated
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { LinearGradient } from "expo-linear-gradient";
import { getPhotoTitle } from "../utils/helperFunctions";

const isOdd = num => num % 2;

export const ImageListItem = props => {
  const {index} = props;
  const [fadeAnim] = useState(new Animated.Value(0));

  React.useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      delay: 300 * index,
      duration: 500,
      useNativeDriver: true
    }).start();
  }, []);

  return (<Animated.View
        style={
          {
            // Bind opacity to animated value
            opacity: fadeAnim
          }
        }
      >
    <TouchableOpacity onPress={props.navigate}>
      <ImageBackground
        style={[
          styles.listItem,
          isOdd(props.index) ? { marginTop: 26 } : { marginBottom: 26 },
          styles.feeImageBackground
        ]}
        imageStyle={{ borderRadius: 10 }}
        source={{
          uri: props.item.urls.small
        }}
      >
        <LinearGradient
          colors={["transparent", "rgba(0,0,0,0.9)"]}
          start={[0.5, 0.7]}
          style={[styles.listItem, styles.absoluteTop]}
        />
        <Text style={styles.feePhotoTitle}>{getPhotoTitle(props.item)}</Text>
        <Text style={styles.feeLikes}>{`${props.item.likes} likes`}</Text>
      </ImageBackground>
    </TouchableOpacity>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  listItem: {
    width: wp("40%"),
    height: wp("55%"),
    borderRadius: 10
  },
  feeImageBackground: {
    justifyContent: "flex-end",
    padding: 10,
    overflow: "hidden"
  },
  absoluteTop: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0
  },
  feePhotoTitle: {
    fontFamily: "MuseoMedium",
    fontSize: 12,
    lineHeight: 14,
    marginBottom: 5,
    color: "#fff"
  },
  feeLikes: {
    fontFamily: "MuseoLight",
    fontSize: 8,
    lineHeight: 9,
    color: "#fff"
  },
});
