import unsplash from "../../utils/Unsplash";
import { UPDATE, UPDATE_PROFILE } from "../../utils/constants";

export const getRandomImages = () => {
    return dispatch =>
      unsplash.photos
          .getRandom({
            count: 16,
            })
            .then(data => dispatch(updateImages(data.response)))
            .catch(err => {
              console.log("Error happened during fetching!", err);
            });
  };
  
  export const getProfileImages = userName => {
    return dispatch =>
      unsplash.users
        .getPhotos({ username: userName, total: 16 })
        .then(data => dispatch(updateProfile(data.response)))
        .catch(err => {
          console.log("Error happened during fetching!", err);
        });
  };

export const updateImages = images => ({
    type: UPDATE,
    payload: images
  });
  
  export const updateProfile = profileImages => ({
    type: UPDATE_PROFILE,
    payload: profileImages
  });