import { UPDATE, UPDATE_PROFILE } from "../../utils/constants";

const defaultState = {
    images: [],
    profileImages: []
  };

  
export const imgsReducer = (state = defaultState, action) => {
    switch (action.type) {
      case UPDATE:
        return { ...state, images: action.payload };
  
      default:
        return state;
    }
  };
  
  export const profileImgsReducer = (state = defaultState, action) => {
    switch (action.type) {
      case UPDATE_PROFILE:
        return { ...state, profileImages: action.payload };
  
      default:
        return state;
    }
  };