import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button } from 'react-native';
import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import "react-native-gesture-handler";
import Fee  from '../screens/Fee';
import Profile from '../screens/Profile';
import Detail from '../screens/Detail';
import { COLORS} from '../utils/colors';
import { NavigationContainer } from '@react-navigation/native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CustomSidebarMenu from '../components/CustomSidebarMenu';
const Drawer = createDrawerNavigator();

export default function DrawerNavHome() {
  return (<NavigationContainer>
        <Drawer.Navigator 
        screenOptions={{
    headerShown: false
  }}
  drawerContent={(props) => <CustomSidebarMenu {...props} />}>
            <Drawer.Screen
                name="Home"

                component={Fee}
                options={{
                    title: 'Fee',
                    drawerIcon: ({ focused, size }) => (
                    <SimpleLineIcons name={'home'} size={size} color={'black'}/>),
                    drawerLabelStyle: styles.headerTitle,
                    headerStyle: styles.headerStyleHome,
                    headerTintColor: COLORS.white,
                    headerStatusBarHeight: 20,
                    headerTitleStyle: styles.headerTitleStyle
                }}
                />
                 <Drawer.Screen
                name="Detail"
                component={Detail}
                options={{
                    title: 'Detail',
                    drawerIcon: ({ focused, size }) => (
                        <Ionicons name={'ios-image-outline'} size={size} color={'black'}/>),
                    drawerLabelStyle: styles.headerTitle,
                    headerStyle: styles.headerStyleHome,
                    headerTintColor: COLORS.white,
                    headerStatusBarHeight: 20,
                    headerTitleStyle: styles.headerTitleStyle
                }}/>
                <Drawer.Screen
                name="Profile"
                component={Profile}
                options={{
                    title: 'Profile',
                    drawerIcon: ({ focused, size }) => (
                        <Ionicons name={'person-outline'} size={size} color={'black'}/>),
                    drawerLabelStyle: styles.headerTitle,
                    headerStyle: styles.headerStyleHome,
                    headerTintColor: COLORS.white,
                    headerStatusBarHeight: 20,
                    headerTitleStyle: styles.headerTitleStyle
                }}/>
        </Drawer.Navigator></NavigationContainer>
  );
}

const styles = StyleSheet.create({
    headerStyleHome: { 
        backgroundColor: COLORS.red_home,
        height: 80,
    },
    headerTitleStyle: { 
        color: COLORS.white,
    },
    headerTitle: { 
        color: COLORS.black,
        fontFamily: "MuseoMedium",
        fontSize: 16,
        lineHeight: 18,
    },

});