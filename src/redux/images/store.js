import { applyMiddleware, createStore, combineReducers } from "redux";
import { configureStore } from '@reduxjs/toolkit'
import thunk from "redux-thunk";
import { getRandomImages } from './actions'
import { imgsReducer, profileImgsReducer } from './reducers'

const middlewares = [thunk];

//-----** Combine Reducers **-----//

const rootReducer = combineReducers({
  getRandomImages,
  images: imgsReducer,
  profileImages: profileImgsReducer
});

//-----** Create Store **-----//

const store = configureStore({
  reducer: rootReducer,
  middleware: middlewares,
})

export default store;