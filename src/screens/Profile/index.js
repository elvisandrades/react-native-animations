import React from "react";
import { FlatList, Image, Text, View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getProfileImages } from "../../redux/images/actions";
import { profileImgsReducer } from "../../redux/images/reducers";
import Header from "../../components/Header";
import { ImageListItem } from "../../components/ImageList";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
  } from "react-native-responsive-screen";
  import {
    HORIZONTAL_PADDING,
    NAVBAR_HEIGHT,
    SCREEN_HEIGHT,
    SCREEN_WIDTH,
    STATUS_BAR_HEIGHT
  } from "../../utils/constants";

class ProfileScreen extends React.Component {
  loadData() {
    const userName = this.props.route.params.item.user.username;
    this.props.getProfileImages(userName);
  }

  close = () => {
    this.props.navigation.goBack(null);
    this.flatListRef.scrollToIndex({ index: 0 });
  };

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.route.params.item.user.name !==
      this.props.route.params.item.user.name
    ) {
      this.loadData();
    }
  }

  render() {
    const { navigation, route } = this.props;
    
    const { item } = route.params;

    return (
      <View style={[styles.flx1, styles.startContent]}>
        <ProfileHeader
          image={item.user.profile_image.medium}
          name={item.user.name}
          bio={item.user.bio}
        />

        <FlatList
          ref={ref => (this.flatListRef = ref)}
          numColumns={2}
          data={this.props.profileImages.profileImages.results}
          keyExtractor={(item, index) => item + index}
          renderItem={({ item, index }) => (
            <ImageListItem
              item={item}
              index={index}
              navigate={() =>
                navigation.navigate("Detail", { data: "profile", index: index })
              }
            />
          )}
          columnWrapperStyle={styles.columnWrapperStyle}
          contentContainerStyle={{
            marginHorizontal: 26
          }}
        />

        <Header
          icon="close"
          onPress={() => this.close()}
          extraStyles={styles.header}
        />
      </View>
    );
  }
}

const ProfileHeader = props => (<View>
  <View style={[styles.profileContainer, styles.profileHeader]}>
    <Image style={styles.profileImage} source={{ uri: props.image }} />
    <View style={styles.profileHeaderText}>
      <Text style={styles.profileUserName}>{props.name}</Text>
      <View style={styles.profileDescription}>
        <Text style={styles.profileDescription}>{props.bio}</Text>
      </View>
    </View>
  </View>
  <View>
    <Text style={styles.myPhotos}>My Photos</Text>
  </View>
  </View>
);

const mapStateToProps = state => {
  const { profileImages } = state;
  return { profileImages };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getProfileImages,
      profileImgsReducer
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileScreen);

const styles = StyleSheet.create({
    flx1: { flex: 1, backgroundColor: 'white'},
    endContent: { justifyContent: "flex-end" },
    startContent: { justifyContent: "flex-start" },
    fullFill: {
        height: SCREEN_HEIGHT,
        width: SCREEN_WIDTH
      },
      header: {
        height: NAVBAR_HEIGHT,
        paddingTop: STATUS_BAR_HEIGHT,
    },
      absoluteTop: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0
      },
      profileTouchable: {
        position: "absolute",
        top: 90,
        bottom: 90,
        left: 90,
        right: 90
      },
      detailInfo: {
        padding: HORIZONTAL_PADDING
      },
      detailPhotoTitle: {
        fontFamily: "MuseoMedium",
        fontSize: 42,
        lineHeight: 49,
        color: "#fff"
      },
      detailLikes: {
        fontFamily: "MuseoLight",
        fontSize: 14,
        lineHeight: 16,
        color: "#fff"
      },
      detailUserName: {
        fontFamily: "MuseoMedium",
        fontSize: 12,
        lineHeight: 14,
        paddingBottom: 8,
        color: "#fff"
      },
      viewProfile: {
        fontFamily: "MuseoRegular",
        fontSize: 10,
        lineHeight: 12,
        color: "#fff"
      },
      //-----** Profile **-----//
    profileContainer: {
    flexDirection: "row",
    paddingTop: 27,
    paddingLeft: HORIZONTAL_PADDING,
    marginRight: HORIZONTAL_PADDING,
    marginBottom: 10,
  },
  userImage: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  columnWrapperStyle: {
    flex: 1,
    justifyContent: "space-between"
  },
  profileHeader: {
    marginTop: hp("11%")
  },
  profileHeaderText: {
    paddingLeft: 12,
    justifyContent: "center"
  },
  profileImage: {
    width: 64,
    height: 64,
    borderRadius: 32
  },
  profileUserName: {
    fontFamily: "MuseoMedium",
    fontSize: 22,
    lineHeight: 26,
    paddingBottom: 8,
    color: "#000"
  },
  profileDescription: {
    fontFamily: "MuseoLight",
    fontSize: 12,
    lineHeight: 14,
    paddingBottom: 8,
    paddingRight: HORIZONTAL_PADDING,
    color: "#000"
  },
  myPhotos: {
    fontFamily: "MuseoLight",
    fontSize: 42,
    color: "#000",
    fontWeight: 'bold',
    paddingLeft: 25,
    paddingBottom: 25
  }
});